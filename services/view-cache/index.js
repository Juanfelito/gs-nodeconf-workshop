const express = require('express')
const request = require('request-promise-native')
const {readOrLoadData} = require('./read-through-cache')

const router = new express.Router()


router.get('/get-data', async (req, res)  => {
    const {key} = req.query
     const data = await  request.get('http://back-end-service:4001/data')
    res.setHeader('Content-Type', 'application/json')
    res.send(data)

})


router.get('/env', async (req, res)  => {
    const {key} = req.query
    // const data = await  request.get(`http://${key}/data`)
    res.setHeader('Content-Type', 'application/json')
    // res.send(data)
    res.send(JSON.stringify(process.env))
})

/*
NOTE - each of these get-data functions will have different load characteristics along with the
read through cache which also has some options - these are especially interesting when you slow services down.

 */

// awaits

// router.get('/get-data', async (req, res)  => {
//     const {key} = req.query
//     const data = await readOrLoadData(key, 'http://back-end:4001/data')
//     res.setHeader('Content-Type', 'application/json')
//     res.send(data)
// })


//
// router.get('/get-data', (req, res) => {
//     const {key} = req.query
//     const data = readOrLoadData(key, 'http://back-end:4001/data')
//     res.setHeader('Content-Type', 'application/json')
//     res.send(data)
// })





const server = express()

server.use(router)

server.listen(4000)